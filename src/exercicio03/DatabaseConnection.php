<?php
namespace Exercicio03;

class DatabaseConnection 
{	
	private $host;
	private $username;
	private $password;
	private $dbname;
	private $db;

	/**
     * Construtor.
     *
     * @return mixed
     */
	public function __construct($host, $username, $password, $dbname)
	{
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->dbname = $dbname;
		$this->db = $this->connect($host, $username, $password, $dbname);
	}

	/**
     * Método responsável por criar a conexão com o banco de dados.
     *
     * @return Object
     */	
	private function connect() 
	{	
	    try {  
            $this->db = new \PDO("mysql:host=".$this->host.";dbname=".$this->dbname, $this->username, $this->password);  
            $this->db->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            $this->db->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false );
            return $this->db;
        }
        catch(\PDOException $e) { 
            echo( 'ERRO: ' . $e->getMessage() );
        } 
		
	}

	/**
     * Método responsável por executar a query especificada.
     *
     * @return Object
     */	
	public function query($sql)
	{
	    $stmt = $this->db->prepare( $sql );
	    $stmt->execute();
	    $stmt->setFetchMode( \PDO::FETCH_OBJ );

	    return $stmt;
	}

}