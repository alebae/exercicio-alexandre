<?php
namespace Exercicio03;

class MyUserClass
{
	private $db;

	/**
     * Construtor.
     *
     * @return mixed
     */
	public function __construct()
	{
		$this->db = new DatabaseConnection(getenv('HOST'), 
										   getenv('USERNAME'), 
										   getenv('PASSWORD'), 
										   getenv('DBNAME'));
	}

	/**
     * Método responsável por listar os usuários por nome e em ordem ascendente.
     *
     * @return Object
     */
	private function getUserList()
	{
		$results = $this->db->query('SELECT name FROM users ORDER BY id ASC');
		return $results;
	}

	/**
     * Método responsável por escrever na tela a relação de usuários
     *
     * @return string
     */
	public function testUserList()
	{
		$results = $this->getUserList();

		foreach( $results as $r ){
			echo $r->name."<br />";
		}
	}
}