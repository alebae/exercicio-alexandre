<?php
namespace Exercicio02;

class Exercicio
{
	private $session;
	private $cookie;
	private $key;
	private $value;

	/**
     * Construtor.
     *
     * @return mixed
     */
	public function __construct($key, $value)
	{
		$this->key = $key;
		$this->value = $value;

		$this->session = $this->createSession($this->key, $this->value);
		$this->cookie = $this->createCookie($this->key, $this->value);
	}

	/**
     * Método responsável por criar a sessão especificada.
     *
     * @return array
     */
	private function createSession($key, $value)
	{
		$sess = new Session();
		$sess[$key] = $value;
		return $sess;
	}

	/**
     * Método responsável por criar o cookie especificado.
     *
     * @return Object
     */
	private function createCookie($key, $value)
	{
		return new Cookie($key, $value);
	}

	/**
     * Método responsável por verificar a existência do cookie.
     *
     * @return boolean
     */
	private function hasCookie($key)
	{
		if ( $this->cookie->getName() === $key ) {
			return true;
		} else {
			return false;
		}
	}

	/**
     * Método responsável por verificar a existência da session.
     *
     * @return boolean
     */
	private function hasSession($key)
	{
		if ( isset( $this->session[$key] ) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
     * Método responsável por comparar a existência de cookies e sessions setados e efetuar a ação de redirecionamento.
     *
     * @return mixed
     */
	public function comparacao($key)
	{
		if ( $this->hasSession($key) || $this->hasCookie($key) ) {
			return header('Location: http://www.google.com');
		} else {
			throw new \Exception('Ação inválida!');
		}
	}
}