<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Exercicio03\\' => array($baseDir . '/src/exercicio03'),
    'Exercicio02\\' => array($baseDir . '/src/exercicio02'),
);
