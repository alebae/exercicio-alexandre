<?php
/**
  * Função que verifica se um valor é múltiplo
  *
  * @return string
  */
function multiplo($valor, $multiplo, $texto)
{
    if (((int)$valor % (int)$multiplo) == 0) {
	    return $texto;
	}
}

/**
  * Função que verifica se o número é multiplo de 3
  *
  * @return string
  */
function multiploTres($number)
{
	$multiploTres = multiplo($number, 3, 'Fizz');
	return $multiploTres;
}

/**
  * Função que verifica se o número é multiplo de 5
  *
  * @return string
  */
function multiploCinco($number)
{
	$multiploCinco = multiplo($number, 5, 'Buzz');
	return $multiploCinco;
}

/**
  * Função que escreverá os termos Fizz, Buzz ou o número, conforme especificações
  *
  * @return string
  */
function escreveMultiplo($m3, $m5, $number)
{
	$str = '';
	if($m3){		
		$str .= $m3;
	}
	if($m5){
		$str .= $m5;
	}

	return (!$str) ? $number : $str;
}

/**
  * Função que montará as funções dos múltiplos
  *
  * @return string
  */
function montaNumeral($number)
{
	$m3 = multiploTres($number);
	$m5 = multiploCinco($number);
	
	echo escreveMultiplo($m3, $m5, $number);

	echo '<br />';	
}	

//Loop de 1 a 100, utilizando a função montaNumeral que informará se é múltiplo de 3 e/ou 5
array_map('montaNumeral', range(1, 100));