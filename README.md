# EXERCÍCIOS - ALEXANDRE FEUSTEL BAEHR (1, 2 e 3)   

# Instruções:  

1) Clonar o repositório:
```bash  
$ git clone https://alebae@bitbucket.org/alebae/exercicio-alexandre.git  
```

2) Navegar até a pasta exercicio-alexandre:
```bash  
$ cd exercicio-alexandre  
```

3) Executar o composer:
```bash   
$ composer install  
```

4) Abrir no navegador:  

a) Exercício 1: http://localhost/exercicio-alexandre/exercicio01.php  
b) Exercício 2: http://localhost/exercicio-alexandre/exercicio02.php  
c) Exercício 3: http://localhost/exercicio-alexandre/exercicio03.php

OBS: Para o exercício 3 será necessário o banco de dados MySQL, criando a tabela:
```sql
CREATE DATABASE IF NOT EXISTS `exercicio` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `exercicio`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `exercicio`.`users` (`id`, `name`) VALUES (1, 'Maria');
INSERT INTO `exercicio`.`users` (`id`, `name`) VALUES (2, 'Pedro');
```